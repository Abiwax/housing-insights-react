import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import './index.css';

import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {reducers} from './service/cometchat/reducer';

import App from './components/App/App';
import {
    Route,
    BrowserRouter as Router, Switch
} from "react-router-dom";
import * as serviceWorker from './serviceWorker';
import Home from "./components/Home/Home";
import StockChatty from "./components/StockChatty/StockChatty";
import CCManager from './service/cometchat/ccManager';

const rootReducers = combineReducers(reducers);
const store = createStore(rootReducers, applyMiddleware(thunk));

CCManager.init(store.dispatch);

const routing = (
    <Router>
        <div>
            <Provider store={store}>
                <App>
                    <Switch>
                        <Route path="/" exact component={Home}/>
                        {/*<Route path="/stock" component={StockChatty}/>*/}
                        <Route path="/stock" render={(props) => <StockChatty {...props}/>}/>
                    </Switch>
                </App>
            </Provider>
        </div>
    </Router>
)

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
