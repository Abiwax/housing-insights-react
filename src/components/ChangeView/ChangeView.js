import React, {Component} from 'react';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import Icon from '@material-ui/core/Icon';
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

const useStyles = makeStyles(theme => ({
    toggleContainer: {
        margin: theme.spacing(2, 0),
        textAlign: 'right'
    },
    toggleButton: {
        marginLeft: '10px'
    }
}));

const ColorIcon = withStyles({
    colorPrimary: {
        color: '#0276B4',
    },
})(Icon);

class ChangeView extends Component {
    state = {
        view: "chart"
    };

    handleView = (event, newView) => {
        this.setState({
            ...this.state,
            view: newView,
        });
    };

    render() {
        const ToggleView = () => {
            const classes = useStyles();

            return (
                <div className={classes.toggleContainer}>
                    <span>Change View</span>
                    <ToggleButtonGroup value={this.props.view} exclusive onChange={this.props.handleView}  className={classes.toggleButton}>
                        <ToggleButton value="chart">
                            <ColorIcon color='primary'>multiline_chart</ColorIcon>
                        </ToggleButton>
                        <ToggleButton value="table">
                            <ColorIcon color='primary'>table_chart</ColorIcon>
                        </ToggleButton>
                        {/*<ToggleButton value="map">*/}
                        {/*    <Icon>map</Icon>*/}
                        {/*</ToggleButton>*/}
                    </ToggleButtonGroup>
                </div>
            );
        };
        return (
            <Row>
                <Col alignment='right'>
                    <ToggleView/>
                </Col>
            </Row>
        )
    }
}

export default ChangeView;
