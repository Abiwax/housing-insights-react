import React from 'react';
import PropTypes from 'prop-types';
import styles from './MapView.scss';

const MapView = props => (
	<div>This is a component called MapView.</div>
);

// todo: Unless you need to use lifecycle methods or local state,
// write your component in functional form as above and delete
// this section. 
// class TableView extends React.Component {
//   render() {
//     return <div>This is a component called TableView.</div>;
//   }
// }

const MapViewPropTypes = {
	// always use prop types!
};

MapView.propTypes = MapViewPropTypes;

export default MapView;
