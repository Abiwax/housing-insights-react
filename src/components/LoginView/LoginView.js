import React from 'react'
import PropTypes from "prop-types";
import testuser1 from '../../assets/images/testuser1.png';
import testuser2 from '../../assets/images/testuser2.png';
import testuser3 from '../../assets/images/testuser3.png';
import {Card, Col, Row} from "react-bootstrap";
import User from "../User/User";
import {ColorLinearProgress} from "../../service/Utils";

function LoadingView(props) {
    if (props.loading) {
        return <ColorLinearProgress/>
    } else {
        return null
    }
}
class LoginView extends React.Component {
    constructor(props) {
        super(props);
        let user_list = [{
            userTitle: 'Test User 1',
            userName: 'testuser1',
            userImage: testuser1
        }, {userTitle: 'Test User 2', userName: 'testuser2', userImage: testuser2}, {
            userTitle: 'Test User 3',
            userName: 'testuser3',
            userImage: testuser3
        }]

        this.state = {
            user_list: user_list
        };
    };

    render() {
        const styles = {
            user: {
                justifyContent: 'center'
            }
        };

        return (
            <Row style={styles.user}>
                <LoadingView loading={this.props.loading}/>
                <Col md={12}> Choose from the list of demo users below to begin chat.</Col>

                {
                    this.state.user_list.map((user) => (
                        <Col key={user.userName} md={3} float="center">
                            <Card>
                                <Card.Body>
                                    <User children={user.userTitle} image={user.userImage} unreadCount={0}
                                          username={user.userName}
                                          uid={user.userName}
                                          login={true} setUserInfo={this.props.setUserInfo}/>
                                </Card.Body>
                            </Card>
                        </Col>
                    ))
                }
            </Row>
        )
    }
};

LoginView.propTypes = {
    messages: PropTypes.node,
};

export default LoginView;
