import React from 'react';
import {Card, Col, Row} from "react-bootstrap";
import {Line} from "react-chartjs-2";
import {SingleStructure} from "../../service/Utils";


// todo: Unless you need to use lifecycle methods or local state,
// write your component in functional form as above and delete
// this section.

class ChartsView extends React.Component {
    generateColor() {
        return '#' + Math.random().toString(16).substr(-6);
    }

    genLineData = (moreData = {}) => {
        let shown_data = this.props.type !== 'stocks' ? this.props.type === 'current' ? this.props.info.bed_data : this.props.info.predicted_data : this.props.info.stock_data;
        return {
            labels: shown_data.length > 0 ? this.props.type !== 'stocks' ? shown_data[0].HousePrices.map((price) => {
                return price.date;
            }) : JSON.parse(shown_data[0].Dates).map((date) => {
                return new Date(date).toLocaleDateString()
            }) : ['Jan', 'Feb'],
            datasets: shown_data.map((res) => {
                let current_color = this.generateColor();
                return {
                    label: this.props.type !== 'stocks' ? this.props.info.by_state ? res.Metro : res.State : res.Symbol,
                    backgroundColor: current_color,
                    borderColor: current_color,
                    borderWidth: 1,
                    data: this.props.type !== 'stocks' ? res.HousePrices.map((price) => {
                        return price.price;
                    }) : JSON.parse(res.Close),
                    ...moreData,
                }
            })
        };
    };

    render() {
        return (
            <Row>
                <Col xl={12} lg={12} md={12}>
                    <Card>
                        <Card.Body>
                            <Card.Title>{
                                this.props.type !== 'stocks' ?
                                    <span>Monthly Data By Location({this.props.info.state}) -
                                        <SingleStructure info={this.props.info}/>
                                    </span> :
                                    <span>Stock Close Data By Company({this.props.info.symbol})</span>
                            } </Card.Title>
                            <Card>
                                <Line data={this.genLineData({fill: false})}/>
                            </Card>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        )
    }
}

export default ChartsView;
