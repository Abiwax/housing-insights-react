import React, {Component} from 'react';
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {makeStyles} from '@material-ui/core/styles';
import {SnackbarContent} from "@material-ui/core";
import Button from '@material-ui/core/Button';
import {FormControl, InputGroup} from "react-bootstrap";
import {ColorLinearProgress} from "../../service/Utils";

const useStyles = makeStyles(theme => ({
    button: {
        width: '100%',
        backgroundColor: '#0276B4',
        color: 'white',
        verticalAlign: 'middle',
        marginTop: '32px',
        "&:hover": {
            backgroundColor: '#0276B4',
        }
    },
    snackbar: {
        backgroundColor: '#0276B4'
    }
}));


class SearchView extends Component {
    handleChange = e => {
        this.props.info[e.target.name] = e.target.value;
    };

    render() {
        const ButtonStyle = () => {
            const classes = useStyles();
            return <Button variant="contained" color="primary" className={classes.button}
                           onClick={() => this.props.submitStockRequest()}>
                Submit
            </Button>
        };
        const SnackbarStyle = () => {
            const classes = useStyles();
            return <SnackbarContent className={classes.snackbar}
                                    message={this.props.info.stock_error}/>
        };

        return (

            <Row>
                <Col md={12}>
                    {
                        this.props.info.stock_loading ?
                            <ColorLinearProgress/> : this.props.info.stock_error ?
                            <SnackbarStyle/> : null
                    }
                </Col>
                <br/>
                <Col md={5}>
                    <label htmlFor="symbol">Stock Symbol</label>
                    <InputGroup className="mb-3">
                        <FormControl name={'symbol'} id={'symbol'} type="text" placeholder="Search for stock values (e.g. AAPL)" onChange={this.handleChange} required/>
                    </InputGroup>

                </Col>
                <Col md={5}>
                    <label htmlFor="days">Days</label>
                    <InputGroup className="mb-3">
                        <FormControl name={'days'} id={'days'} type="number" placeholder="Enter number of days (e.g 10)" onChange={this.handleChange} required/>
                        <InputGroup.Append>
                            <InputGroup.Text>days</InputGroup.Text>
                        </InputGroup.Append>
                    </InputGroup>
                </Col>
                <Col md={2}>
                    <ButtonStyle/>
                </Col>
            </Row>
        )
    }
}

export default SearchView;

