import React from 'react';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import DataView from "../DataView/DataView";
import {makeStyles, useTheme} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';


import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import {SnackbarContent} from "@material-ui/core";

function TabPanel(props) {
    const {children, value, index, ...other} = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            <Box p={3}>{children}</Box>
        </Typography>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}

const useStyles = makeStyles(theme => ({
    root: {
        backgroundColor: 'white',
        flexGrow: 1,
    },
    tab: {
        backgroundColor: 'white',
        boxShadow: 'none',
        borderBottom: '#C1BDBD 1px'
    },
    snackbar: {
        backgroundColor: '#0276B4'
    },
}));

export default function FullWidthTabs() {
    const classes = useStyles();
    const theme = useTheme();
    const tab_theme = createMuiTheme({
        palette: {
            textColor: '#0276B4',
        },
        overrides: {
            MuiTabs: {
                indicator: {
                    backgroundColor: '#0276B4',
                    color: '#0276B4'
                }
            },
            MuiTab: {
                textColorPrimary: {
                    color: 'black'
                },
                wrapper: {
                    color: "#0276B4"
                },
                root: {
                    color: '#0276B4',
                    "&:hover": {
                        color: '#0276B4',
                    },
                    "&:selected": {
                        backgroundColor: "#0276B4",
                        color: "#0276B4"
                    }
                }
            }
        }
    });

    const [value, setValue] = React.useState(0);

    function handleChange(event, newValue) {
        setValue(newValue);
    }

    function handleChangeIndex(index) {
        setValue(index);
    }

    return (
        <MuiThemeProvider theme={tab_theme}>
            <div className={classes.root}>
                <AppBar position="static">
                    <SnackbarContent className={classes.snackbar}
                                     message="This site displays house data prices in the United States and allows for prediction of future prices. Please note that this site is just for demo purposes and prediction has not been optimized. Frameworks used includes React and Django."/>
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        textColor='primary'
                        aria-label="full width tabs example"
                        className={classes.tab}
                    >
                        <Tab label="Current Prices (2015-2016)" {...a11yProps(0)} />
                        <Tab label="Predict Future Prices" {...a11yProps(1)} />
                    </Tabs>
                </AppBar>
                <SwipeableViews
                    axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                    index={value}
                    onChangeIndex={handleChangeIndex}
                >
                    <TabPanel value={value} index={0} dir={theme.direction}>
                        <DataView type='current'/>
                    </TabPanel>
                    <TabPanel value={value} index={1} dir={theme.direction}>
                        <DataView type='predict'/>
                    </TabPanel>
                </SwipeableViews>
            </div>
        </MuiThemeProvider>
    );
};