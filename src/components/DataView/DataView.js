import React, {Component} from 'react';
import FilterView from "../FilterView/FilterView";
import ChartsView from "../ChartsView/ChartsView";
import {api_call_get, api_call_post} from "../../service/ApiCalls";
import {SnackbarContent} from "@material-ui/core";
import TableView from "../TableView/TableView";
import ChangeView from "../ChangeView/ChangeView";

class DataView extends Component {
    state = {
        view: "chart",
        loading: true,
        error: '',
        data: [],
        state: "all",
        structure: "all",
        bed_data: [],
        predicted_data: [],
        structures: [],
        states: [],
        metros: [],
        counties: [],
        metro: "all",
        county: "all",
        start_date: new Date(),
        end_date: new Date(),
        max_date: new Date(),
        min_date: new Date(),
        min_start_date: new Date(),
        by_state: false
    };

    constructor(props) {
        super(props);

        this.PAGINATION_SIZE_DEFAULT = 10;
        this.PAGINATION_START = 0;
        this.PAGINATION_END = this.PAGINATION_SIZE_DEFAULT;
        this.PAGINATION_PAGE_SIZES = [10, 20, 30];

        this.getRooms();
    }

    getRooms = () => {
        let key = "structures";
        let api_key = "structures";
        api_call_get('series', api_key, null, null).then((result) => {
            let data = result.data;
            if (data.length > 0) {
                let single_data = data[0];
                let key_bed = "bed_data";
                api_call_get('series', api_key, `${single_data.id}/`, null).then((result) => {
                    let house_prices = [];
                    let state = 'all';
                    if (result.data.structure_prices && result.data.structure_prices.length > 0) {
                        house_prices = result.data.structure_prices;
                    }
                    this.setState({
                        ...this.state,
                        [key_bed]: house_prices,
                        'structure': single_data.id,
                        'state': state,
                        'loading': false
                    });
                    this.filterSelects('structure', single_data.id, single_data.id)
                }).catch((err) => {
                    this.setState({
                        ...this.state,
                        [key_bed]: [],
                        'error': "Could not connect to server to get single data.",
                        'loading': false
                    });
                })
                let start_date = new Date(single_data.end_date);
                start_date.setMonth(start_date.getMonth() + 1);
                let end_date = new Date();
                end_date.setFullYear(end_date.getFullYear() + 5);

                let start_d = this.props.type === 'current' ? new Date(single_data.start_date) : start_date;
                let end_d = this.props.type === 'current' ? new Date(single_data.end_date) : end_date;

                let min_start_date = new Date(Object.assign({}, {start_date: start_d}).start_date);

                this.setState({
                    ...this.state,
                    'start_date': start_d,
                    'end_date': end_d,
                    'min_start_date': min_start_date,
                    'min_date': Object.assign({}, {"start": start_d}).start,
                    'max_date': Object.assign({}, {"end": end_d}).end
                });
            }
            this.setState({
                ...this.state,
                [key]: result.data
            });
        }).catch((err) => {
            this.setState({
                ...this.state,
                [key]: [],
                'error': "Could not connect to server.",
                'loading': false
            });
        })
    };

    submitRooms = (predict = false) => {
        let key = predict ? "predicted_data" : "bed_data";
        let api_key = "structures";
        this.setState({
            ...this.state,
            'loading': true
        });

        let submission = {
            "predict": predict,
            "State": this.state.state,
            "Metro": this.state.metro,
            "CountyName": this.state.county,
            "start_date": `${this.state.start_date.getFullYear()}-${this.state.start_date.getMonth() + 1}`,
            "end_date": `${this.state.end_date.getFullYear()}-${this.state.end_date.getMonth() + 1}`
        };

        api_call_post('series', api_key, submission, `${this.state.structure}/`).then((result) => {
            let house_prices = result.data;
            this.setState({
                ...this.state,
                [key]: house_prices,
                'loading': false
            });
        }).catch((err) => {
            this.setState({
                ...this.state,
                [key]: [],
                'error': "Could not connect to server to get data.",
                'loading': false
            });
        })

    };

    handleView = (event, newView) => {
        this.setState({
            ...this.state,
            view: newView,
        });
    };

    getCounties = (current_structure) => {
        return current_structure.structure_prices.filter((structure) => {
            if (this.state.metro !== 'all' && this.state.state !== 'all') {
                return structure['Metro'] === this.state.metro && structure['State'] === this.state.state
            } else if (this.state.metro === 'all' && this.state.state !== 'all') {
                return structure['State'] === this.state.state
            } else if (this.state.metro !== 'all') {
                return structure['Metro'] === this.state.metro
            }
            return true;
        }).map((structure) => {
            return structure.CountyName;
        });
    };

    getMetros = (current_structure) => {
        return current_structure.structure_prices.filter((structure) => {
            if (this.state.state !== 'all') {
                return structure['State'] === this.state.state
            }
            return true;
        }).map((structure) => {
            return structure.Metro;
        });
    };

    getState = (current_structure) => {
        return current_structure.structure_prices.filter((structure) => {
            return true;
        }).map((structure) => {
            return structure.State;
        });
    };

    filterSelects = (type, selected, selected_structure) => {
        if (type !== 'start_date' && type !== 'end_date') {
            let name = type === 'CountyName' ? 'county' : type === 'State' ? 'state' : type === 'Metro' ? 'metro' : 'structure';

            let selected_structure_object = this.state.structures.filter((structure) => {
                return structure.id.toString() === selected_structure.toString();
            });

            if (selected_structure_object.length > 0) {
                let current_structure = selected_structure_object[0];

                let states = [];
                let metros = [];
                let counties = [];

                if (type === 'structure') {
                    if (selected === 'all') {
                    } else {
                        states = current_structure.structure_prices.map((structure) => {
                            return structure.State;
                        });
                        metros = current_structure.structure_prices.map((structure) => {
                            return structure.Metro;
                        });
                        counties = current_structure.structure_prices.map((structure) => {
                            return structure.CountyName;
                        });

                        this.setState({
                            ...this.state,
                            [name]: selected,
                            'state': this.props.type === 'current' ? 'all' : states[0],
                            'metro': this.props.type === 'current' ? 'all' : metros[0],
                            'county': this.props.type === 'current' ? 'all' : counties[0],
                        });
                    }
                } else {
                    if (type.toLowerCase() === 'state' && selected === 'all') {
                        states = this.getState(current_structure);

                        metros = this.getMetros(current_structure);

                        counties = this.getCounties(current_structure);
                    } else if (type.toLowerCase() === 'metro' && selected === 'all') {
                        states = this.getState(current_structure);

                        metros = this.getMetros(current_structure);

                        counties = this.getCounties(current_structure);

                    } else if (type.toLowerCase() === 'countyname' && selected === 'all') {
                        states = this.getState(current_structure);

                        metros = this.getMetros(current_structure);

                        counties = this.getCounties(current_structure);

                    } else {
                        if (type !== 'State' && type !== 'Metro' && type !== 'CountyName') {
                            states = current_structure.structure_prices.filter((structure) => {
                                return structure[type] === selected;
                            }).map((structure) => {
                                return structure.State;
                            });
                        } else {
                            states = this.state.states;

                        }

                        if (type !== 'Metro' && type !== 'CountyName') {
                            metros = current_structure.structure_prices.filter((structure) => {
                                return structure[type] === selected;
                            }).map((structure) => {
                                return structure.Metro;
                            });
                        } else {
                            metros = this.state.metros;
                        }

                        if (type !== 'CountyName') {
                            counties = current_structure.structure_prices.filter((structure) => {
                                return structure[type] === selected;
                            }).map((structure) => {
                                return structure.CountyName;
                            });
                        } else {
                            counties = this.state.counties;
                        }
                    }
                }

                if (type === 'State') {
                    this.setState({
                        ...this.state,
                        [name]: selected,
                        'metro': this.props.type === 'current' ? 'all' : metros[0],
                        'county': this.props.type === 'current' ? 'all' : counties[0],
                        'states': [...new Set(states)],
                        'metros': [...new Set(metros)],
                        'counties': [...new Set(counties)],
                    });
                } else if (type === 'Metro') {
                    this.setState({
                        ...this.state,
                        [name]: selected,
                        'county': this.props.type === 'current' ? 'all' : counties[0],
                        'states': [...new Set(states)],
                        'metros': [...new Set(metros)],
                        'counties': [...new Set(counties)],
                    });
                } else {
                    this.setState({
                        ...this.state,
                        [name]: selected,
                        'states': [...new Set(states)],
                        'metros': [...new Set(metros)],
                        'counties': [...new Set(counties)],
                    });
                }
            }
        } else {
            this.setState({
                ...this.state,
                [type]: selected,
            });
        }
        if (this.props.type === 'current') {
            setTimeout(() => this.submitRooms())
        }

    };
    handleChange = (name) => event => {
        this.filterSelects(name, name !== 'start_date' && name !== 'end_date' ? event.target.value : event, name === 'structure' ? event.target.value : this.state.structure);
    };

    render() {

        return (
            <div>
                <FilterView info={this.state} view={this.props.type} handleChange={this.handleChange}
                            submitRooms={this.submitRooms}/>
                <ChangeView view={this.state.view} handleView={this.handleView}/>
                {
                    this.state.view === 'chart' ? this.props.type === 'predict' && this.state.predicted_data.length < 1 ?

                        <SnackbarContent message="Click on Submit to get predictions.
                                    If you have clicked on submit and data does not show, it means we could not predict your data."/>

                        : <ChartsView info={this.state} type={this.props.type}/> :
                        <TableView info={this.state} type={this.props.type}/>
                }
            </div>
        )

    }
}

export default DataView;
