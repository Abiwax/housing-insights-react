import React from 'react';
import PropTypes from 'prop-types';
import {faUserCircle} from "@fortawesome/free-solid-svg-icons";
import {Row} from "react-bootstrap";

const User = (props) => {
    const {image, username, children, uid, login, status, unreadCount, setUserInfo, ...other} = props;

    var unreadBadge = login ? null : unreadCount > 0 ? ( <div className="unreadCounter_div"><span className="unreadBadge"> {unreadCount}</span></div>):null;
    return (
        <div key={uid} onClick={setUserInfo.bind(this, username)}>
            <Row className={'userItem'}>
                    <span className="sidebarUserListItemAvatar">
                        <img alt={username} className="userAvatar img-circle" src={image ? image : faUserCircle} height="40px"
                             width="40px"/>
                    </span>

                <div className="sidebarUserListItemTitle">
                    <span>{children}</span>
                </div>
                {
                    login ? null : <div className="sidebarUserListItemStatus" data={this.props.status}>
                        <span>{status}</span>
                    </div>
                }


                {unreadBadge}


            </Row>
        </div>
    );
};

User.propTypes = {
    other: PropTypes.node,
    status: PropTypes.node,
    setUserInfo: PropTypes.func,
    image: PropTypes.any.isRequired,
    unreadCount: PropTypes.any.isRequired,
    username: PropTypes.string.isRequired,
    uid: PropTypes.any.isRequired,
    children: PropTypes.any.isRequired,
    login: PropTypes.bool.isRequired,
};

export default User;
