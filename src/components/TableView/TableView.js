import React from 'react';
import {MDBDataTable} from 'mdbreact';
import {Card} from "react-bootstrap";
import {SingleStructure} from "../../service/Utils";

// todo: Unless you need to use lifecycle methods or local state,
// write your component in functional form as above and delete
// this section. 
class TableView extends React.Component {
    constructor(props) {
        super(props);
        this.getData()
    }

    data = {
        columns: [
            {
                label: 'Date',
                field: 'date',
                sort: 'asc',
                width: 150
            }
        ],
        rows: [{}]
    };

    getData() {
        let extra_columns = [];
        if (this.props.type !== 'stocks') {
            extra_columns = [
                {
                    label: 'State',
                    field: 'state',
                    sort: 'asc',
                    width: 270
                },
                {
                    label: 'Metro',
                    field: 'metro',
                    sort: 'asc',
                    width: 270
                },
                {
                    label: 'County',
                    field: 'county',
                    sort: 'asc',
                    width: 200
                },
                {
                    label: 'Average Price',
                    field: 'price',
                    sort: 'asc',
                    width: 200
                }
            ];

            let shown_data = this.props.type === 'current' ? this.props.info.bed_data : this.props.info.predicted_data;

            let actual_data = shown_data.map((data) => {
                let single_info = {state: data.State, metro: data.Metro, county: data.CountyName};

                return data.HousePrices.map((data) => {
                    let single_data = Object.assign({date: data.date}, single_info);
                    single_data.price = data.price;
                    return single_data
                });
            });
            this.data.rows = [].concat.apply([], actual_data);
        } else {
            extra_columns = [
                {
                    label: 'Company Symbol',
                    field: 'symbol',
                    sort: 'asc',
                    width: 370
                },
                {
                    label: 'Open',
                    field: 'open',
                    sort: 'asc',
                    width: 370
                },
                {
                    label: 'Close',
                    field: 'close',
                    sort: 'asc',
                    width: 370
                }
            ];
            this.data.rows = this.props.info.stock_table_data;
        }
        this.data.columns = [].concat.apply(this.data.columns, extra_columns);

    }

    render() {
        return (
            <Card>
                <Card.Body>
                    <Card.Title>
                        {
                            this.props.type !== 'stocks' ? <span>Monthly Data By Location({this.props.info.state}) - <SingleStructure
                                info={this.props.info}/></span> : <span>Stock Open and Close Data By Company({this.props.info.symbol})</span>
                        }
                    </Card.Title>
                    <Card>
                        <MDBDataTable
                            striped
                            bordered
                            hover
                            data={this.data}/>
                    </Card>
                </Card.Body>
            </Card>

        )

    }
}

export default TableView;
