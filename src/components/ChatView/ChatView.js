import React from 'react'
import PropTypes from "prop-types";
import SwipeableBottomSheet from 'react-swipeable-bottom-sheet';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faChevronDown, faChevronUp} from "@fortawesome/free-solid-svg-icons";
import LoginView from "../LoginView/LoginView";
import {connect} from 'react-redux';
import {isEmpty} from "../../service/Utils";
import * as actionCreator from '../../service/cometchat/cc_action';

class ChatView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            loading: false,
        };
    };

    openBottomSheet(open) {
        this.setState({open});
    };

    toggleBottomSheet() {
        this.openBottomSheet(!this.state.open);
    };

    setUserInfo = (user) => {
        this.setState({
            loading: true
        });

        if (!this.state.loading) {
            console.log("user : ", user);
            this.props.setUserSession(user);
        }

    }

    render() {
        const styles = {
            swipe: {
                width: '70%',
                float: 'right',
                left: 'auto',
                right: '10px'
            },
            title: {
                backgroundColor: '#0276B4',
                padding: '16px 0',
                boxSizing: 'border-box',
                color: 'white',
                minHeight: '64px',
                fontSize: '14px',
                textAlign: 'center',
                flex: 1
            },
            text: {
                padding: '16px 0',
                boxSizing: 'border-box',
                backgroundColor: 'white',
                fontSize: '18px',
                minHeight: '50vh',
                verticalAlign: 'middle',
                textAlign: 'center',
                paddingLeft: '10px',
                justifyContent: 'center',
                paddingRight: '10px'
            }
        };


        return (
            <SwipeableBottomSheet
                overflowHeight={74}
                open={this.state.open}
                onChange={this.openBottomSheet.bind(this)}
                style={styles.swipe}
            >
                <div style={styles.title}>
                    <div>
                        <FontAwesomeIcon icon={this.state.open ? faChevronDown : faChevronUp}
                                         onClick={this.toggleBottomSheet.bind(this)}/>
                    </div>
                    <div>
                        Chat Room
                    </div>
                </div>
                <div style={styles.text} className={'text-center'}>
                    <CheckLoginUser loading={this.state.loading}  loggedStatus={this.props.loggedInUser} setUserInfo={this.setUserInfo}/>
                </div>
            </SwipeableBottomSheet>
        )
    }
};

ChatView.propTypes = {
    messages: PropTypes.node,
};

const mapStateToProps = state => {
    return {

        loggedInUser: state.users.loggedInUser
    };
};

const mapDispachToProps = dispatch => {
    return {
        setUserSession: (object) => dispatch(actionCreator.loginInCC(dispatch, object)),
    };
};

export default connect(
    mapStateToProps,
    mapDispachToProps
)(ChatView);

function CheckLoginUser(props) {
    if (isEmpty(props.loggedStatus)) {
        console.log("show login screen");
        return <LoginView setUserInfo={props.setUserInfo} loading={props.loading}/>;
    } else {
        console.log("show Messenger");
        return <span>Here</span>;
    }

}
