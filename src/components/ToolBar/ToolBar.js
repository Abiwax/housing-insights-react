import React from 'react';
import {Navbar, NavbarBrand, Nav} from 'react-bootstrap';
import './ToolBar.scss';
import logo from '../../assets/images/Logo.png';
// todo: Unless you need to use lifecycle methods or local state,
// write your component in functional form as above and delete
// this section.
import {withRouter} from "react-router-dom";

class ToolBar extends React.Component {
    render() {
        return (
            <Navbar bg="light" variant="light" fixed="top">
                <NavbarBrand href="/">
                    <img src={logo} className="LogoImage" alt="displayed file page" id="logo"/>
                    {' Presola Demo Applications'}
                </NavbarBrand>
                <Nav className="mr-auto" activeKey={this.props.location.pathname}>
                    <Nav.Item>
                        <Nav.Link href="/">Housing Insights</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link href="/stock">Stock Chatty</Nav.Link>
                    </Nav.Item>
                </Nav>
            </Navbar>
        )
    }
}

export default withRouter(ToolBar);
