import React from 'react';
import { shallow, render, mount } from 'enzyme';
import ToolBar from './ToolBar';

describe('ToolBar', () => {
  let props;
  let shallowToolBar;
  let renderedToolBar;
  let mountedToolBar;

  const shallowTestComponent = () => {
    if (!shallowToolBar) {
      shallowToolBar = shallow(<ToolBar {...props} />);
    }
    return shallowToolBar;
  };

  const renderTestComponent = () => {
    if (!renderedToolBar) {
      renderedToolBar = render(<ToolBar {...props} />);
    }
    return renderedToolBar;
  };

  const mountTestComponent = () => {
    if (!mountedToolBar) {
      mountedToolBar = mount(<ToolBar {...props} />);
    }
    return mountedToolBar;
  };  

  beforeEach(() => {
    props = {};
    shallowToolBar = undefined;
    renderedToolBar = undefined;
    mountedToolBar = undefined;
  });

  // Shallow / unit tests begin here
 
  // Render / mount / integration tests begin here
  
});
