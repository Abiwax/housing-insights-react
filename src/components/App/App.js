import React from 'react';
import './App.css';
import ToolBar from '../ToolBar/ToolBar'
import {Container} from 'react-bootstrap';

const App = props => {
    return (
        <div className='MainClass'>
            <ToolBar/>
            <br/>
            <br/>
            <Container fluid={true}>
                {props.children}
            </Container>
        </div>
    );
};

export default App;
