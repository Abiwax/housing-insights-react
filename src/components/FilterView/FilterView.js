import React, {Component} from 'react';
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {makeStyles, withStyles} from '@material-ui/core/styles';
import {FormControl, SnackbarContent} from "@material-ui/core";
import InputLabel from "@material-ui/core/InputLabel";
import InputBase from '@material-ui/core/InputBase';
import NativeSelect from '@material-ui/core/NativeSelect';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import {SingleStructure} from "../../service/Utils";

const BootstrapInput = withStyles(theme => ({
    root: {
        'label + &': {
            marginTop: theme.spacing(3),
        },
    },
    input: {
        borderRadius: 4,
        position: 'relative',
        backgroundColor: theme.palette.background.paper,
        border: '1px solid #ced4da',
        fontSize: 16,
        padding: '10px 26px 10px 12px',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        // Use the system font instead of the default Roboto font.
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        '&:focus': {
            borderRadius: 4,
            borderColor: '#80bdff',
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
        },
    },
}))(InputBase);

const useStyles = makeStyles(theme => ({
    button: {
        backgroundColor: '#0276B4',
        color: 'white',
        verticalAlign: 'middle',
        marginTop: '15px',
        "&:hover": {
            backgroundColor: '#0276B4',
        }
    },
    snackbar: {
        backgroundColor: theme.palette.error.dark
    }
}));

function SnackbarStyle() {
    const classes = useStyles();
    return <SnackbarContent className={classes.snackbar}
                            message={this.props.info.error}/>
};

function LoadingView(props) {
    if (props.info.loading) {
        return <SingleStructure info={props.info}/>
    } else {
        if (props.info.error) {
            return <SnackbarStyle/>
        } else {
            return <span></span>;
        }
    }
};

class FilterView extends Component {

    render() {
        const ButtonStyle = () => {
            const classes = useStyles();
            return <Button variant="contained" color="primary" className={classes.button}
                           onClick={() => this.props.submitRooms(this.props.view !== 'current')}>
                Submit
            </Button>
        };

        return (

            <Row>
                <Col md={12}>
                    <LoadingView info={this.props.info}/>
                </Col>
                <Col>
                    <FormControl>
                        <InputLabel shrink htmlFor="structure-simple">Structure</InputLabel>
                        <NativeSelect
                            value={this.props.info.structure}
                            onChange={this.props.handleChange('structure')}
                            input={<BootstrapInput name="structure" id="structure-simple"/>}
                        >
                            {this.props.info.structures.map((structure) => (
                                <option value={structure.id} key={structure.id}>{structure.name}</option>
                            ))}
                        </NativeSelect>
                    </FormControl>
                </Col>
                <Col>
                    <FormControl>
                        <InputLabel shrink htmlFor="state-simple">State</InputLabel>
                        <NativeSelect
                            value={this.props.info.state}
                            onChange={this.props.handleChange('State')}
                            input={<BootstrapInput name="state" id="state-simple"/>}
                        >
                            {this.props.view === 'current' ? <option value="all">All</option> : null}
                            {this.props.info.states.map((state) => (
                                <option value={state} key={state}>{state}</option>
                            ))}
                        </NativeSelect>
                    </FormControl>
                </Col>
                <Col>
                    <FormControl>
                        <InputLabel shrink htmlFor="metro-simple">Metro</InputLabel>
                        <NativeSelect
                            value={this.props.info.metro}
                            onChange={this.props.handleChange('Metro')}
                            input={<BootstrapInput name="metro" id="metro-simple"/>}
                        >
                            {this.props.view === 'current' ? <option value="all">All</option> : null}
                            {this.props.info.metros.map((metro) => (
                                <option value={metro} key={metro}>{metro}</option>
                            ))}
                        </NativeSelect>
                    </FormControl>
                </Col>
                <Col>
                    <FormControl>
                        <InputLabel shrink htmlFor="county-simple">County</InputLabel>
                        <NativeSelect
                            value={this.props.info.county}
                            onChange={this.props.handleChange('CountyName')}
                            input={<BootstrapInput name="county" id="county-simple"/>}
                        >
                            {this.props.view === 'current' ? <option value="all">All</option> : null}
                            {this.props.info.counties.map((county) => (
                                <option value={county} key={county}>{county}</option>
                            ))}
                        </NativeSelect>
                    </FormControl>
                </Col>
                <Col>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} id="start-simple">
                        <Grid container justify="space-around">
                            <KeyboardDatePicker
                                views={["year", "month"]}
                                margin="normal"
                                id="mui-pickers-date"
                                label="Start Date"
                                minDate={this.props.info.min_date}
                                maxDate={this.props.info.max_date}
                                value={this.props.info.start_date}
                                onChange={this.props.handleChange('start_date')}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </Grid>
                    </MuiPickersUtilsProvider>
                </Col>
                <Col>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} id="end-simple">
                        <Grid container justify="space-around">
                            <KeyboardDatePicker
                                views={["year", "month"]}
                                margin="normal"
                                id="mui-pickers-date"
                                label="End Date"
                                minDate={this.props.view === 'current' ? this.props.info.start_date : this.props.info.min_start_date}
                                maxDate={this.props.info.max_date}
                                value={this.props.info.end_date}
                                onChange={this.props.handleChange('end_date')}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </Grid>
                    </MuiPickersUtilsProvider>
                </Col>
                {this.props.view !== 'current' ? <Col> <ButtonStyle/> </Col> : null}
            </Row>
        )
    }
}

export default FilterView;

