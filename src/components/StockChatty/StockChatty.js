import React, {Component} from 'react';
import ChartsView from "../ChartsView/ChartsView";
import {api_call_post} from "../../service/ApiCalls";
import {SnackbarContent} from "@material-ui/core";
import TableView from "../TableView/TableView";
import ChangeView from "../ChangeView/ChangeView";
import SearchView from "../SearchView/SearchView";
import {makeStyles} from '@material-ui/core/styles';
import PropTypes from "prop-types";
import ChatView from "../ChatView/ChatView";

const useStyles = makeStyles(theme => ({
    snackbar: {
        backgroundColor: '#0276B4'
    }
}));


class StockChatty extends Component {
    state = {
        view: "chart",
        stock_loading: false,
        stock_error: '',
        symbol: "",
        days: 1,
        stock_data: [],
        stock_table_data: []
    };

    submitStockRequest = () => {
        let key = "stock_data";
        let key2 = "stock_table_data";
        let api_key = "stocks";
        this.setState({
            ...this.state,
            'stock_loading': true
        });

        let submission = {
            "query": `{ stocks(symbol: "${this.state.symbol}", days: ${this.state.days}) { Dates Close Symbol }, stocksExpanded(symbol: "${this.state.symbol}", days: ${this.state.days}) { date symbol open close } }`
        };

        api_call_post('stocks', api_key, submission, '').then((result) => {
            let no_error = {'message': ""};
            let error = result.data.errors ? result.data.errors[0] ? result.data.errors[0] : no_error : no_error;
            let stock_prices = result.data ? result.data.data ? result.data.data.stocks ? result.data.data.stocks : [] : [] : [];
            let stock_table_prices = result.data ? result.data.data ? result.data.data.stocksExpanded ? result.data.data.stocksExpanded : [] : [] : [];
            this.setState({
                ...this.state,
                [key]: stock_prices ? stock_prices : [],
                [key2]: stock_table_prices ? stock_table_prices : [],
                'stock_loading': false,
                'stock_error': error['message'] ? error['message'] : ''
            });
        }).catch((err) => {
            this.setState({
                ...this.state,
                [key]: [],
                [key2]: [],
                'stock_error': "Could not connect to server to get data.",
                'stock_loading': false
            });
        })

    };

    handleView = (event, newView) => {
        this.setState({
            ...this.state,
            view: newView,
        });
    };

    render() {
        const SnackbarStyle = (props) => {
            const {message} = props;
            const classes = useStyles();
            return <SnackbarContent className={classes.snackbar}
                                    message={message}/>
        };
        SnackbarStyle.propTypes = {
            message: PropTypes.node,
        };
        return (
            <div>
                <SnackbarStyle message="Search for stocks data and chat at the same time with other users. To search for multiple company stock data, use a comma seperated list (AAPL, HPQ). Maximum Search Submission is 5 per day. Built Using React, Django, GraphQL and CometChat"/>
                <SearchView info={this.state} submitStockRequest={this.submitStockRequest}/>
                <ChangeView view={this.state.view} handleView={this.handleView}/>
                {
                    this.state.view === 'chart' ? this.props.type === 'predict' && this.state.predicted_data.length < 1 ?

                        <SnackbarContent message="Click on Submit to get predictions.
                                    If you have clicked on submit and data does not show, it means we could not predict your data."/>

                        : <ChartsView info={this.state} type={'stocks'}/> :
                        <TableView info={this.state} type={'stocks'}/>
                }
                <ChatView/>
            </div>
        )

    }
}

export default StockChatty;
