import React from 'react';
import { shallow, render, mount } from 'enzyme';
import ChartsView from './HeaderFooter';

describe('ChartsView', () => {
  let props;
  let shallowHeaderFooter;
  let renderedHeaderFooter;
  let mountedHeaderFooter;

  const shallowTestComponent = () => {
    if (!shallowHeaderFooter) {
      shallowHeaderFooter = shallow(<ChartsView {...props} />);
    }
    return shallowHeaderFooter;
  };

  const renderTestComponent = () => {
    if (!renderedHeaderFooter) {
      renderedHeaderFooter = render(<ChartsView {...props} />);
    }
    return renderedHeaderFooter;
  };

  const mountTestComponent = () => {
    if (!mountedHeaderFooter) {
      mountedHeaderFooter = mount(<ChartsView {...props} />);
    }
    return mountedHeaderFooter;
  };  

  beforeEach(() => {
    props = {};
    shallowHeaderFooter = undefined;
    renderedHeaderFooter = undefined;
    mountedHeaderFooter = undefined;
  });

  // Shallow / unit tests begin here
 
  // Render / mount / integration tests begin here
  
});
