import React from "react";
import {withStyles} from "@material-ui/core";
import LinearProgress from "@material-ui/core/LinearProgress";

export class SingleStructure extends React.Component {
    single_data = 'All';

    constructor(props) {
        super(props);
        this.getSingle()
    }

    getSingle() {
        let mapped_list = this.props.info.structures.filter((info) => {
            return info.id === this.props.info.structure;
        }).map((info) => {
            return info.name
        });
        if (mapped_list.length > 0) {
            this.single_data = mapped_list[0]
        }
    }

    render() {
        return (
            <span>
                {this.single_data}
            </span>
        )
    }
}

const ColorProgress = withStyles({
    colorPrimary: {
        backgroundColor: 'rgba(2,118,180,0.18)',
    },
    barColorPrimary: {
        backgroundColor: '#0276B4',
    },
})(LinearProgress);

export const ColorLinearProgress = (props) => {
    return <ColorProgress/>
};

/**function isEmpty
 *
 * Input : Any object
 * output : boolean
 */
export function isEmpty(obj) {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
            return false;
    }

    return true;
}
