import axios from 'axios';
import endpoints from '../data/api.json';

let main_url = process.env.API_PROTOCOL && process.env.API_IP && process.env.API_PORT ? `${process.env.API_PROTOCOL}://${process.env.API_IP}:${process.env.API_PORT}/` : endpoints.url;

function api_call_get(main_path, path, extras, body) {
    return new Promise((resolve, reject) => {
        if (main_url) {
            let url = `${main_url}${main_path}${endpoints[path]}`;
            if (extras) {
                url = `${url}${extras}`
            }
            axios.get(url, body).then((result) => {
                resolve(result)
            }).catch((err) => {
                reject(err)
            });
        } else {
            reject({success: false})
        }

    })

}


function api_call_post(main_path, path, body, extra_path) {
    return new Promise((resolve, reject) => {
        if (main_url) {
            let url = `${main_url}${main_path}${endpoints[path]}${extra_path}`;
            axios.post(url, body).then((result) => {
                resolve(result)
            }).catch((err) => {
                reject(err)
            });
        } else {
            reject({success: false})
        }

    })

}

export {
    api_call_get,
    api_call_post
};
