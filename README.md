# Demo Applications
Sample React Demo Applications

## Applications
#### Housing Insights
===================

Web application built Frontend using React and Backend using Django Rest Framework and Swagger.
Functionalities includes:

1. Get current house prices in different location, locations have been prepopulated.
2. Predict future house prices in different locations. Due to the current data available for just a few years, prediction may stay on a straight line for a while.
3. Data is visualized either via a table or chart.

#### Stock Chatty
===================

A simple stock application that lets user chat while viewing company's closing stock.

Functionalities includes:

1. Get stock prices of a particular company based on a number of days.
2. Chat with other logged in users

## Requirement
1. To run this application install the requirements in the package.json file.
```bash
    npm install
```
2. Update the `data/api.json` file to include your backend URL.
3. Backend repo can be found here [Django Backend](https://github.com/presola/Insights)

## Run
Once all the requirements have been installed, navigate to this folder in a command line or terminal and run the below commands.

#### Local

In the project directory, you can run:

```bash
    npm start
```

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


#### Deploy on Heroku
[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://github.com/presola/ReactSamples)
